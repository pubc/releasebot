package main

import (
	"bytes"
	"encoding/json"
	"regexp"
	"strings"
)

func TrimNewLines(str string) string {
	reg := regexp.MustCompile(` +|\n+`)
	tmpStr := reg.ReplaceAllString(str, " ")
	return strings.TrimSpace(tmpStr)
}

func FixJsonNullField(jsonData []byte) []byte {
	tmpStr := strings.ReplaceAll(string(jsonData), "\"alt\":{\"content\":\"\"},", "")
	return []byte(tmpStr)
}

func MarshalNoEscape(v interface{}) ([]byte, error) {
	buffer := &bytes.Buffer{}
	encoder := json.NewEncoder(buffer)
	// Set not to escape HTML characters
	encoder.SetEscapeHTML(false)

	err := encoder.Encode(v)
	if err != nil {
		return nil, err
	}

	str := buffer.String()
	// The Encode method will add a newline character at the end
	str = strings.TrimRight(str, "\n")

	return []byte(str), nil
}

func BoldSeverity(str string) string {
	reg := regexp.MustCompile(`[Cc]ritical|[Hh]igh|[Mm]edium|[Ll]ow|[Ii]nformational`)
	return reg.ReplaceAllString(str, "- **$0**")
}
