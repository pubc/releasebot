package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
)

type releaseBot struct {
	ReleaseTitle string `json:"release_title"`
	ReleaseLink  string `json:"release_link"`
	ReleaseDesc  string `json:"release_desc"`
	ReleaseCover string `json:"release_cover"`
}

func main() {
	if len(os.Args) < 2 {
		log.Fatalf("Incorrect usage:\n  ./release-bot <release public url>\n")
	}

	url := os.Args[1]

	regPubic := regexp.MustCompile(`/gitlab-\d+-\d+-released`)
	regPatch := regexp.MustCompile(`/gitlab-\d+-\d+-\d+-released`)

	var cardJsonBytes, cover string
	if regPubic.MatchString(url) {
		DownloadFromGitLab(url)
		cover, _ = UploadToFeiShu()
		cardJsonBytes = Features(url)
	} else if regPatch.MatchString(url) {
		cardJsonBytes = Patches(url)
		cover = "img_v3_02ac_896bc1aa-5fc1-4522-ac39-f8e564810c9g"
	} else {
		cardJsonBytes = Security(url)
		cover = "img_v3_02ac_a8c6f75e-38c2-4060-b889-dd2224d50b2g"
	}

	rb := releaseBot{
		Title(url),
		url,
		cardJsonBytes,
		cover,
	}

	jsonData, _ := MarshalNoEscape(rb)

	fmt.Println(string(jsonData))
}
