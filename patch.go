package main

import (
	"fmt"
	"github.com/antchfx/htmlquery"
	"log"
	"regexp"
	"strings"
)

func Patches(url string) string {
	doc, err := htmlquery.LoadURL(url)
	if err != nil {
		log.Fatal(err)
	}

	nodes := htmlquery.Find(doc, "//h2")
	targetTitle := "GitLab Community Edition and Enterprise Edition"

	var patchText string
	for _, node := range nodes {
		if htmlquery.InnerText(node) == targetTitle {
			nextNode := node.NextSibling
			for nextNode != nil && nextNode.Data != "h2" {
				reg := regexp.MustCompile(`\d+.\d+.\d+`)
				text := htmlquery.InnerText(nextNode)
				if reg.MatchString(text) {
					patchText += fmt.Sprintf("\n**%s**\n", TrimNewLines(text))
				} else if strings.TrimSpace(text) != "" {
					trs := strings.Split(text, "\n")
					for _, tr := range trs {
						if strings.TrimSpace(tr) != "" {
							patchText += fmt.Sprintf("- %s\n", TrimNewLines(tr))
						}
					}
				}
				nextNode = nextNode.NextSibling
			}
			break
		}
	}

	return strings.Trim(patchText, "\n")
}
