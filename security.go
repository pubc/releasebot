package main

import (
	"fmt"
	"github.com/antchfx/htmlquery"
	"log"
	"strings"
)

func Security(url string) string {

	doc, err := htmlquery.LoadURL(url)
	if err != nil {
		log.Fatal(err)
	}

	tables, err := htmlquery.QueryAll(doc, "//table")
	if err != nil {
		log.Fatal(err)
	}

	var tableText string
	for _, table := range tables {
		tableTextTr := htmlquery.InnerText(table)
		tmpStr := strings.ReplaceAll(tableTextTr, "\n", " ")
		trs := strings.Split(tmpStr, "  ")
		for _, tr := range trs {
			if strings.TrimSpace(tr) != "" && !strings.Contains(tr, "Severity") {
				tableText += fmt.Sprintf("- %s\n", BoldSeverity(TrimNewLines(tr)))
			}
		}
	}

	return tableText
}
