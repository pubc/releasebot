package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/antchfx/htmlquery"
	lark "github.com/larksuite/oapi-sdk-go/v3"
	larkcore "github.com/larksuite/oapi-sdk-go/v3/core"
	larkim "github.com/larksuite/oapi-sdk-go/v3/service/im/v1"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

// UploadToFeiShu SDK 使用文档：https://github.com/larksuite/oapi-sdk-go/tree/v3_main
// 复制该 Demo 后, 需要将 "YOUR_APP_ID", "YOUR_APP_SECRET" 替换为自己应用的 APP_ID, APP_SECRET.
func UploadToFeiShu() (string, error) {
	// 创建 Client
	client := lark.NewClient("cli_a6a139599020100c", "qTx8Bz8awkz6agi6xv8dWgGroN4K1ASo")
	file, err := os.Open("cover.png")
	if err != nil {
		fmt.Println(err)
		return "", fmt.Errorf("open file \"cover.png\" failed: %s", err)
	}
	defer file.Close()

	// 创建请求对象
	req := larkim.NewCreateImageReqBuilder().
		Body(larkim.NewCreateImageReqBodyBuilder().
			ImageType(`message`).
			Image(file).
			Build()).
		Build()

	// 发起请求
	resp, err := client.Im.Image.Create(context.Background(), req)

	// 处理错误
	if err != nil {
		fmt.Println(err)
		return "", fmt.Errorf("create context failed: %s", err)
	}

	// 服务端错误处理
	if !resp.Success() {
		return "", fmt.Errorf("%d %s %s", resp.Code, resp.Msg, resp.RequestId())
	}

	// 业务处理
	lines := strings.Split(larkcore.Prettify(resp), "\n")
	for _, l := range lines {
		if strings.Contains(l, "ImageKey") {
			return strings.Trim(strings.Fields(l)[1], "\""), nil
		}
	}
	return "", nil
}

// DownloadFromGitLab 从 gitlab release 下载图片
func DownloadFromGitLab(url string) {
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	resp, err := client.Get(url)
	if err != nil {
		log.Fatalf("Request failed: %s", err)
	}
	defer resp.Body.Close()

	doc, err := htmlquery.Parse(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	// 查找包含 background-image 属性的元素
	nodes, err := htmlquery.QueryAll(doc, "//*[contains(@style, 'background-image')]")
	if err != nil {
		log.Fatal(err)
	}

	for _, node := range nodes {
		// 提取 background-image 属性的值
		style := htmlquery.SelectAttr(node, "style")
		url := extractBackgroundImageURL(style)
		ImageDownload(fmt.Sprintf("https://about.gitlab.com%s", url))
	}
}

func ImageDownload(url string) {
	// 替换为背景图片的URL
	filePath := "cover.png" // 保存到本地的文件路径

	// 发送HTTP请求获取图片数据
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	// 创建本地文件用于保存图片
	file, err := os.Create(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// 将HTTP响应的图片数据保存到本地文件
	_, err = io.Copy(file, resp.Body)
	if err != nil {
		log.Fatal(err)
	}
}

// 从 style 字符串中提取 background-image 的 URL
func extractBackgroundImageURL(style string) string {
	startIndex := strings.Index(style, "url(")
	endIndex := strings.Index(style, ")")
	if startIndex == -1 || endIndex == -1 {
		return ""
	}
	return style[startIndex+4 : endIndex]
}
