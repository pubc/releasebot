package main

import (
	"fmt"
	"github.com/antchfx/htmlquery"
	"log"
	"strings"
)

func Features(url string) string {

	doc, err := htmlquery.LoadURL(url)
	if err != nil {
		log.Fatal(err)
	}

	nodes, err := htmlquery.QueryAll(doc, "//h2")
	if err != nil {
		log.Fatal(err)
	}

	var features string
	for _, node := range nodes {
		title := htmlquery.InnerText(node)
		anchor := htmlquery.SelectAttr(node, "id")
		features += fmt.Sprintf("- [%s](%s/#%s)\n", TrimNewLines(title), strings.TrimSuffix(url, "/"), anchor)
	}

	return features
}
