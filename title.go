package main

import (
	"github.com/antchfx/htmlquery"
	"log"
)

func Title(url string) string {
	doc, err := htmlquery.LoadURL(url)
	if err != nil {
		log.Fatal(err)
	}

	h1 := htmlquery.FindOne(doc, "//h1")
	if h1 != nil {
		text := htmlquery.InnerText(h1)
		return text
	}
	return ""
}
